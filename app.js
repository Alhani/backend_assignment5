require('dotenv').config()
let express = require('express')
let app = express()
let bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let mysql = require('mysql2/promise')
let port = process.env.PORT;
let user = process.env.USER;
let host = process.env.HOST;
let passweord = process.env.PASSWORD;
let database = process.env.DATABASE;

let cont;
async function userDatabase() {
    cont = await mysql.createConnection({
        user: user,
        host: host,
        password: passweord,
        database: database
    })
}
userDatabase()
app.post('/contact', async (req, res) => {
    let name = req.body.first_name;
    let lastName = req.body.last_name;
    let phoneNumber = req.body.phone_number;
    let workNumber = req.body.work_number;

    let sql = "INSERT INTO users (first_name, last_name, phone_number, work_number) VALUES (?, ?, ?, ?)";
    try {
        let [result, data] = await cont.query(sql, [name, lastName, phoneNumber, workNumber])
        res.json(result)

    } catch (error) {
        console.log(error);
        res.json({ massage: error })
    }



})
app.get('/contact/:id', async (req, res) => {
    let id = req.params.id
    try {

        const [row, filed] = await cont.execute(`SELECT * FROM users WHERE id=${id}`)
        if (row.length > 0) {

            res.json(row)
            console.log(row);
        }
        else {
            res.json({ massage: 'this id does nor exist' })

        }

    }
    catch (error) {
        res.send(error)
    }
})
app.put('/contact/:id', async (req, res) => {
    let id = req.params.id;
    // let dataup = req.body;
    let name = req.body.first_name;
    let lastName = req.body.last_name;
    let phoneNumber = req.body.phone_number;
    let workNumber = req.body.work_number;

    const [row, filed] = await cont.execute(`SELECT * FROM users WHERE id=${id}`)
    if (row.length > 0) {
        let updateSql =`UPDATE users SET first_name=? , last_name=? , phone_number=? , work_number=? WHERE id=${id}`
        // let updateSql = `UPDATE users SET (first_name, last_name, phone_number, work_number) VALUES (?, ?, ?, ?) WHERE id=${id}`;
        try {
            let [result, data] = await cont.query(updateSql, [name, lastName, phoneNumber, workNumber]);
            res.json(result)
        } catch (error) {
            console.log(error);
            res.json({ massage: error });
        }
    }
    else {
        res.json({ massage: 'This id does not exist' })

    }


})
app.delete('/contact/:id', async (req, res) => {

    let id = req.params.id
    let deleteSql = `DELETE FROM users WHERE id=?`

    const [row, filed] = await cont.execute(`SELECT * FROM users WHERE id=${id}`)
    if (row.length > 0) {
        try {
            let [result, data] = await cont.query(deleteSql, [id])
            res.json({result,     message: 'Contact deletec successfully.'})

        } catch (error) {
            console.log(error);
            res.json({ massage: error });
        }
    }
    else {
        res.json({ massage: 'This id does not exist' })

    }

})



app.get('/contact', async (req, res) => {
    try {

        const [row, filed] = await cont.execute(`SELECT * FROM users`)

        res.json(row)
        console.log(row);
    }
    catch (error) {
        console.log(error);
        res.json({ massage: error });
    }
})
app.listen(port, () => {
    console.log(` http://localhost:${port}/`);
})



    // {"first_name": "Farid", "last_name": "Hamm", "phone_number": "+88761327753","work_number": "Null"}
    // {"first_name": "john", "last_name": "brown", "phone_number": "+989125741123","work_number": "+1889712389"}